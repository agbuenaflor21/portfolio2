

$(document).ready(function(e){
	$win= $(window);
	$navbar = $('#header');
	$toggle = $('.toggle-button');
	var width = $navbar.width();
	toggle_onclick($win,$navbar,width);

	//resize event
	$win.resize(function(){
		toggle_onclick($win,$navbar,width);
	})

	$toggle.click(function(e){
		$navbar.toggleClass("toggle-left");
	})

});

function toggle_onclick($win, $navbar, width){
	if($win.width()<=768){
		$navbar.css({left: `-${width}px`});
	}else{
		$navbar.css({left: '0px'});
	}
}

var typed = new Typed('#typed',{
	strings: [
		'Im a Web Developer',
		'Im a Designer',
		'I embrace change',
		'I have the courage to innovate',
		'I have a passion for growth',
		'I am a Leader and a Team Player',
	],
	typeSpeed: 50,
	backSpeed: 50,
	loop: true
});

var typed_2 = new Typed('#typed_2',{
	strings: [
		'Im a Web Developer',
		'I embrace change',
		'I have the courage to innovate',
		'I have a passion for growth',
		'I am a Leader and a Team Player',

	],
	typeSpeed: 50,
	backSpeed: 50,
	loop: true
});

var typed_3 = new Typed('#typed_3',{
	strings: [
		'Welcome to my portfolio',
		'I need help to clear these particles by hovering the mouse'
	],
	typeSpeed: 50,
	backSpeed: 50,
	loop: true
});




// Projects

$(document).ready(function() {
  $("#myCarousel").on("slide.bs.carousel", function(e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });
});
